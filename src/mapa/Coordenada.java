package mapa;

import mapa.Singleton.Cuadrante;

public class Coordenada {

    public int x, y;

    public Coordenada(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getCuadrante() {
        if (x >= 97 && x <= 259 && y >= 120 && y <= 270) return Cuadrante.SUP_IZQ;
        if (x >= 97 && x <= 259 && y >= 360 && y <= 510) return Cuadrante.INF_IZQ;
        if (x >= 569 && x <= 731 && y >= 120 && y <= 270) return Cuadrante.SUP_DER;
        if (x >= 569 && x <= 731 && y >= 360 && y <= 510) return Cuadrante.INF_DER;
        if (x >= 300 && x <= 528 && y >= 210 && y <= 420) return Cuadrante.CENTRO;
        return -1;
    }

    public double distanciaEuclideaDesde(Coordenada otra) {
        double a = this.x - otra.x;
        a = Math.pow(a, 2);
        double b = this.y - otra.y;
        b = Math.pow(b, 2);
        return Math.sqrt(a + b);
    }
}
