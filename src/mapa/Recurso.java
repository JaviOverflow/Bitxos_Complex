package mapa;

public class Recurso extends Coordenada {

    private double tiempo;

    public Recurso(int x, int y, float tiempo) {
        super(x, y);
        this.tiempo = tiempo;
    }

    public double getTiempo() {
        return tiempo;
    }
}
