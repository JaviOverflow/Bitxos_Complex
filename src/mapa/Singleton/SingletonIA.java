package mapa.Singleton;

import mapa.Coordenada;
import mapa.Recurso;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class SingletonIA {

    private static SingletonIA INSTANCE = null;

    private LinkedHashMap<Integer, Cuadrante> mCuadrantes;

    private SingletonIA() {

        mCuadrantes.put(Cuadrante.SUP_IZQ, new Cuadrante(Cuadrante.SUP_IZQ));
        mCuadrantes.put(Cuadrante.SUP_DER, new Cuadrante(Cuadrante.SUP_DER));
        mCuadrantes.put(Cuadrante.INF_DER, new Cuadrante(Cuadrante.INF_DER));
        mCuadrantes.put(Cuadrante.INF_IZQ, new Cuadrante(Cuadrante.INF_IZQ));
        mCuadrantes.put(Cuadrante.CENTRO, new Cuadrante(Cuadrante.CENTRO));

    }

    public static SingletonIA getInstance() {
        return INSTANCE;
    }

    public static void initInstance() {
        INSTANCE = new SingletonIA();
    }

    public void actualizarMapa(ArrayList<? extends Coordenada> sensores, ArrayList<? extends Coordenada> recursos) {

        eliminarObstaculos(sensores, false);
        eliminarObstaculos(recursos, true);

    }

    public void siguienteMovimientoHaciaRecurso(Integer giro, Boolean moverse, Coordenada posicionBicho,
                                                Integer orientacionBicho, ArrayList<Recurso> recursos) {

        ArrayList<Recurso> recursosMeta = new ArrayList<Recurso>();
        recursosMasCercanos(3, recursosMeta, recursos, posicionBicho);

        for (Recurso r : recursosMeta) {
            // 1.- Obtenemos la ecuación de la recta bicho-recurso

            // 2.- Miramos que obstaculos corta la recta bicho-recurso

            // 3.- Obtenemos la lista de nodosBloque

            // 4.- Trazamos el camino

            // 5.- Calculamos el coste del camino y lo comparamos con el tiempo de vida del recurso
        }

        // Calculamos si hemos de ir a por algun recurso y el ángulo de giro

    }

    /**
     * Compara las coordenadas de (si 'sonRecursos' == False) los sensores con los obstáculos y mira si hay alguno
     * que no contenga dichas coordenadas o de (si 'sonRecursos' == True) los recursos y los obstaculos para ver
     * si se solapan y poder eliminar obstaculos de los cuadrantes.
     *
     * @param coordenadas
     * @param sonRecursos
     */
    private void eliminarObstaculos(ArrayList<? extends Coordenada> coordenadas, boolean sonRecursos) {

        for (Coordenada coordenada : coordenadas) {
            mCuadrantes.get(coordenada.getCuadrante()).compruebaCoordenada(coordenada, sonRecursos);
        }
    }

    /**
     * Busca los tres recursos más cercanos respecto de la posición del bicho.
     *
     * @param cantidad
     * @param recursosMeta
     * @param recursos
     * @param posicionBicho
     */
    private void recursosMasCercanos(int cantidad, ArrayList<Recurso> recursosMeta, ArrayList<Recurso> recursos,
                                     Coordenada posicionBicho) {

        for (int i = 0; i < cantidad; i++) {
            Recurso tmpRecurso = null;
            double tmpDistanciaMinima = Double.MAX_VALUE;
            for (Recurso r : recursos) {
                if (r.distanciaEuclideaDesde(posicionBicho) < tmpDistanciaMinima && !recursosMeta.contains(r)) {
                    tmpRecurso = r;
                }
            }
            recursosMeta.add(tmpRecurso);
        }
    }

}
