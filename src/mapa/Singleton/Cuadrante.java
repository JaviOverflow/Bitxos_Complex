package mapa.Singleton;

import mapa.Coordenada;

import java.util.ArrayList;

public class Cuadrante {

    public static final int SUP_IZQ = 0;
    public static final int SUP_DER = 1;
    public static final int INF_DER = 2;
    public static final int INF_IZQ = 3;
    public static final int CENTRO = 4;

    private ArrayList<Obstaculo> mObstaculos;

    public Cuadrante(int tipo) {

        switch (tipo) {
            case SUP_IZQ: // (11) Esquina superior izquiera
                initCuadranteEsquina(0, 0);
                break;
            case SUP_DER: // (1) Esquina superior derecha
                initCuadranteEsquina(472, 0);
                break;
            case INF_DER: // (5) Esquina inferior derecha
                initCuadranteEsquina(472, 240);
                break;
            case INF_IZQ: // (7) Esquina inferior izquierda
                initCuadranteEsquina(0, 240);
                break;
            case CENTRO: // (Centro) Centro
                initCuadranteCentro();
                break;
        }
    }

    /**
     * Inicializador genérico para la lista de de obstáculos de los cuadrantes situados en las esquinas
     *
     * @param x
     * @param y
     */
    private void initCuadranteEsquina(int x, int y) {

        mObstaculos.add(new Obstaculo(97 + x, 230 + x, 165 + y, 195 + y, 0, 0, 0, 0));
        mObstaculos.add(new Obstaculo(97 + x, 230 + x, 120 + y, 150 + y, 97 + x, 127 + x, 120 + y, 240 + y));
        mObstaculos.add(new Obstaculo(97 + x, 259 + x, 120 + y, 150 + y, 229 + x, 259 + x, 120 + y, 240 + y));

        mObstaculos.add(new Obstaculo(97 + x, 244 + x, 210 + y, 240 + y, 214 + x, 244 + x, 120 + y, 240 + y));
        mObstaculos.add(new Obstaculo(97 + x, 230 + x, 210 + y, 240 + y, 97 + x, 127 + x, 120 + y, 240 + y));
        mObstaculos.add(new Obstaculo(97 + x, 230 + x, 120 + y, 150 + y, 148 + x, 178 + x, 120 + y, 240 + y));

        mObstaculos.add(new Obstaculo(97 + x, 230 + x, 240 + y, 270 + y, 148 + x, 178 + x, 120 + y, 270 + y));
        mObstaculos.add(new Obstaculo(97 + x, 230 + x, 165 + y, 195 + y, 148 + x, 178 + x, 120 + y, 240 + y));
        mObstaculos.add(new Obstaculo(0, 0, 0, 0, 148 + x, 178 + x, 120 + y, 240 + y));
    }

    /**
     * Inicializador de la lista de obstáculos para el cuadrante central
     */
    private void initCuadranteCentro() {

        mObstaculos.add(new Obstaculo(300, 499, 285, 315, 0, 0, 0, 0));
        mObstaculos.add(new Obstaculo(300, 499, 210, 240, 300, 330, 210, 390));
        mObstaculos.add(new Obstaculo(300, 528, 210, 240, 498, 528, 210, 390));

        mObstaculos.add(new Obstaculo(300, 513, 360, 390, 483, 513, 210, 390));
        mObstaculos.add(new Obstaculo(300, 499, 360, 390, 300, 330, 210, 390));
        mObstaculos.add(new Obstaculo(300, 499, 210, 240, 384, 414, 210, 390));

        mObstaculos.add(new Obstaculo(300, 499, 390, 420, 384, 414, 210, 420));
        mObstaculos.add(new Obstaculo(300, 499, 285, 315, 384, 414, 210, 390));
        mObstaculos.add(new Obstaculo(0, 0, 0, 0, 384, 414, 210, 390));
    }

    /**
     * Elimina los obstaculos que no contengan las coordenadas marcadas por los sensores o aquellos
     * que contengan coordenadas en las que hay recursos en el mapa.
     *
     * @param coordenada
     * @param esRecurso
     */
    public void compruebaCoordenada(Coordenada coordenada, boolean esRecurso) {

        for (Obstaculo obstaculo : mObstaculos) {
            if ((esRecurso && obstaculo.contienePunto(coordenada.x, coordenada.y)) ||
                    (!esRecurso && !obstaculo.contienePunto(coordenada.x, coordenada.y))) {
                mObstaculos.remove(obstaculo);
            }
        }
    }

}
