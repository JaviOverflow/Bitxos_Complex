package mapa.Singleton;

import mapa.Coordenada;

public class NodoBloque extends Coordenada {

    public NodoBloque(int x, int y) {
        super(x, y);
    }


    @Override
    public boolean equals(Object obj) {
        return (this.x == ((NodoBloque) obj).x) && (this.y == ((NodoBloque) obj).y);
    }
}
