package mapa.Singleton;

import java.util.ArrayList;

public class Obstaculo {

    private Bloque[] bloques;

    public Obstaculo(int xo1, int xf1, int yo1, int yf1, int xo2, int xf2, int yo2, int yf2) {
        bloques = new Bloque[2];
        bloques[0] = new Bloque(xo1, xf1, yo1, yf1);
        bloques[1] = new Bloque(xo2, xf2, yo2, yf2);
    }

    public boolean contienePunto(int x, int y) {
        return bloques[0].contienePunto(x, y) || bloques[1].contienePunto(x, y);
    }

    public void getNodos(ArrayList<NodoBloque> lista) {
        for (Bloque bloque : bloques) {
            bloque.completarNodos(lista);
        }
    }
}
