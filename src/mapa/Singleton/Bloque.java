package mapa.Singleton;

import java.util.ArrayList;

public class Bloque {

    private int xo, xf, yo, yf;
    private NodoBloque mNodos[];

    public Bloque(int xo, int xf, int yo, int yf) {
        this.xo = xo;
        this.xf = xf;
        this.yo = yo;
        this.yf = yf;

        // Código basura
        this.mNodos[0] = new NodoBloque(xo - 25, yo - 25);
        this.mNodos[1] = new NodoBloque(xo - 25, yf + 25);
        this.mNodos[2] = new NodoBloque(xf + 25, yo - 25);
        this.mNodos[3] = new NodoBloque(xf + 25, yf + 25);
    }

    public boolean contienePunto(int x, int y) {
        return (x >= xo) && (x <= xf) && (y >= yo) && (y <= yf);
    }

    public void completarNodos(ArrayList<NodoBloque> lista) {
        for (NodoBloque n : mNodos) {
            if (!lista.contains(n)) {
                lista.add(n);
            }
        }

    }
}
